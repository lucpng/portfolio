---
title: "Noztra.app"
type: portfolio
date: 2019-05-01T00:00:00+00:00
description : "Noztra is a treasure hunt game around 3D art and Augmented Reality"
caption: Augmented Reality App Made With Unity
image: images/portfolio/noztra.jpg
category: ["Augmented Reality","Web","App","Unity","Game", "C#"]
submitDate: May, 2019
---
### Noztra a Treasure Hunt Game around 3D Art in **Augmented Reality**

Noztra is my first big project. With a group of 6 people working along a year and a half, we achieved to create a prototype of this app.
Its concept is simple :

> To promote 3D art and artists to the general public.

The idea is to encourage artists to create **3D creations** related to a specific place in the "real" world. These would be virtually exposed in the place they meant to be and accessible for everyone via an app using **AR**. Users could look for pieces of art like they were already doing with Pokemons. They could look at them, react to them, follow their makers but also save them to create their own gallery.

We have made it as a Web App, using classic web technologie like JS, HTML, CSS, PHP but also especially Unity which has carried the entire AR part. The real challenge here was to prevent server weakness and network problems.

Finally we setted up an entire communication strategy and learned a lot on commercial problems.
