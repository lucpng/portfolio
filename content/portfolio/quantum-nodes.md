---
title: "Quantum Nodes"
type: portfolio
date: 2021-08-01T00:00:00+00:00
description : "Quantum Nodes a Blender Add-On made for quantum computing based modelling"
caption: Quantum Computing Blender Plugin
image: images/portfolio/quantum-nodes.jpg
category: ["Add-On","Quantum Computing", "SIGGRAPH", "3D modelling", "Procedural modelling", "Procedural animation"]
submitDate: August, 2021
livelink: http://quantum-nodes.com/
---
### An International Project

**Quantum Nodes** is a Blender add-on that gives an access to Quantum Computing in the software. With 5 team mates we achieved to create a really great Blender Tool. 

The idea was was that from the basis of [**Animation Nodes**](https://animation-nodes.com/) we created our own complete add-on to implement quantum algorithm in the 3D creation workflow.

The add-on, thanks to Animation Nodes, allows users to import their data (vertex positions, vertex colors, objects etc...), then to create their own quantum circuit to manipulate these data and finally translate the information from the quantum circuit into the initial data type.

Our project has been presented as a poster to the SIGGRAPH 2021 and has finally been selected.

>You can find our Siggraph abstract [here](https://dl.acm.org/doi/abs/10.1145/3450618.3469155?casa_token=1TYij2QW8ScAAAAA%3A9YySjplzxMrphwuV2DsTvx9EFOMqNsLjdGplvcAyVNMucW35wASJAfPPuU9izSNxOCI3Z5-1VOlqsQ)