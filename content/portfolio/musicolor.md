---
title: "Musicolor"
type: portfolio
date: 2018-11-12T00:00:00+00:00
description : "Art Installation Mixing Music And Painting"
caption: Interactive Installation With Unity And Pure Data
image: images/portfolio/musicolor.jpg
category: ["Unity","Art","3D","Pure Data","C#","Installation","Launchpad","Human Interaction"]
submitDate: November, 2018
---
### Musicolor a **creative** and **interactive** installation

Musicolor is a project made during my studies in **DUT MMI**. We had to use a specific visual scripting tool called **Pure Data**. It's specially made for audio and visual interactive creations. In this case, I decided to put the interaction to the very heart of my creation. I chose to **melt** sound and visual creation in a single experience.

Here, the launchpad will be our interface. It will trigger sounds but also a function into the real-time game engine **Unity** that will cast a paint ball on a canvas.

> The idea here was to combine a music played by the user and its digital vizualisation in a creative way.

The real challenge here was creating the interface between both Pure Data and Unity. Get the user input via Pure data and send info translated in C# script to Unity was not that easy.
But finally the result is here and we were happy of the result.
