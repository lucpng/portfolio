---
title: "Devine's Hell"
type: portfolio
date: 2021-04-01T00:00:00+00:00
description : "Creative and with multiple technics Title Sequence"
caption: Title Sequence With VFX
image: images/portfolio/devine-hell.jpg
category: ["Opening","Music Composition","VFX","3D Modelling", "Animation", "Scripting", "Editing", "Directing"]
liveLink: https://youtu.be/9SrmslJY7S8
submitDate: April, 2021
---
### Devine's Hell a Title Sequence like no other

The project is not the most technical one but he allowed me to expose my creativity and my universe throw different technics.
In the first place, I wrote a TV Show scenario. But instead of directing the all project, the final result would be only its opening. I wanted to use as much as technics as I can, VFX, rotoscopy, music composition, typography and so on...

Hope you'll like the result.
