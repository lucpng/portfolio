---
title: "Point Cloud Handling In Unity"
type: portfolio
date: 2021-08-01T00:00:00+00:00
description : "Internship Project with Point Cloud Animation in Unity"
caption: Visual Effect Graph And Custom Shaders In Unity
image: images/portfolio/monark.jpg
category: ["Unity","VFX Graph","Shader Graph", "Point Cloud", "Particle System", "3D Scan", "3D", "Animation"]
liveLink: https://www.mnrk.io
client: Monark
submitDate: August, 2021
---
### Monark VFX and Point Cloud Animation Project 

During Summer 2021, I had the opportunity to work with a start-up in 3D Scan, Lidar and Photogrammetry technology.
I worked for them on different tasks but mainly on two differents things.

First, I worked on **Unity** with **Visual Effect Graph** and **Shader Graph** to handle Huge Point Clouds and manipulate them. Animations and other treatment for aesthetic purpose. The idea was to exploit Point Cloud precise data and use it to create abstract and distorted world like works of **Ruben Fro or Benjamin Bardou**.

Then, I started to work on a project of point real-time point cloud web renderer meant to exploit this aesthetic. This part of the project is in construction.
