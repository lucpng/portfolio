---
title: "Real-Time Fractal Editor"
type: portfolio
date: 2021-10-01T00:00:00+00:00
description : "A Ray Marching Renderer Made For Real-Time Fractal Editor"
caption: Mandelbulb Editor In Unity -IN PROGRESS-
image: images/portfolio/rt-mandelbulb.jpg
category: ["3D","Fractals", "Unity", "Ray Marching", "Compute Shader", "Tool Programming"]
submitDate: March, 2022

---
### A Real-Time Fractal... Really ?

This project is based on an **AI** course where we studied multiple technics around AIs like genetic algorithms, deep learning, boids and... **fractals**.

Fractals were, for a long time, very costly to compute and manipulate it in real-time was not even imaginable. 
Now, some technics exist and I wanted to try things out around this topics.

That's why, I have made some research on **Ray Marching** Rendering and how implement this type of renderer in Unity with **Compute Shaders**.

Then, with the help of a couple of great resources and github repositories, I am able to create some Mandelbulb in Unity.

Now I would like to implement the editor part and make it a real tool for fractal manipulation.

A **Gitlab** link will be soon available.