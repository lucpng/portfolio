---
title: "Wonderful World"
type: portfolio
date: 2021-01-01T00:00:00+00:00
description : "This is meta description"
caption: OpenGL Game
image: images/portfolio/wonderful-world.jpg
category: ["bag","mockup"]
submitDate: January, 2021
---
### An OpenGl Complete Renderer

Wonderful World was one of the biggest, toughest but also best project of my studies. In a really short time, we had to create a 3D Video Game from scratch in C++ with OpenGL. The important thing was to understand as much as we could about the Graphic Library. I've created with 2 other team mates a complete Mesh renderer using different libraries: SDL, Assimp, Glew and so on...

This project helped me a lot to understand what I like in 3D programming and shader programming in general.
In fact we implemented a Physically Based Rendering with the Cook-Torrance Light Model and to try things out we also added a Deffered Rendering. 

This project was only an exercise but it asked a lot of resources. I learned so much on this topic but also on me that it could the most important one. 