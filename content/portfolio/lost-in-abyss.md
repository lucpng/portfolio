---
title: "Lost In Abyss"
type: portfolio
date: 2021-03-01T00:00:00+00:00
description : "3D Escape Game made with UE4"
caption: Virtual Escape Game Made With UE4
image: images/portfolio/lost-in-abyss.jpg
category: ["3D","Game", "UE4", "Escape Game", "Blueprint", "C++", "Animation", "Post-Processing"]
submitDate: March, 2021
livelink: https://art3mma.itch.io/lost-in-abyss
---
### A Immersive Escape Game

During an english course, we had the opportunity to create an Escape Game with our own method. So, with my group of 4 people, we decided to create a video game with the real-time game engine, Unreal Engine 4.

This game was, for most of us, our first experience on **UE4**, we tried things out. Sometimes it was great, sometimes less but the final result is here and fully functional with a lot of different features: UI, music, dynamic materials, parenting, blueprints, complex algorithms...