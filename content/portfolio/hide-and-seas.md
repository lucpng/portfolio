---
title: "Hide And Seas"
type: portfolio
date: 2022-01-01T00:00:00+00:00
description : "End of studies project, multiple game mode video game"
caption: 3D Game With UE4 - IN PROGRESS -
image: images/portfolio/hide-and-seas.jpg
category: ["Unreal Engine", "C++", "Blueprint", "Game Design", "Level Design", "Algorithm", "AI Behavior"]
liveLink: https://github.com/Thomas-Zorroche/HideAndSeas
submitDate: March, 2022
---
### Hide & Seas, an adventure video game

Hide & Seas is the name of my final project of my studies in IMAC. During 7 months we are working on a video game in C++ helped with **Unreal Engine 4**.

The game is devide in two different gameplays :
One part Adventure / Exploration where you'are on your ship and explore the seas to find the different islands.

The second part takes place on the island. The infiltration gameplay starts and you will have to reach your objective while avoid the guards' gaze.

>With the rest of the team, we wanted to exploit **procedural generation** to create a game playable and replayable anytime you want.

Islands are placed in the world randomly but also infiltration levels are proceduraly generated.

